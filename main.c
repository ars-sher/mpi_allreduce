#include <stddef.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include "mpi.h"
#include "all_reduce_max.h"

int main(int argc, char **argv) 
{
  int  rank, size = 99;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size); // now size = number of processes
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); // now rank = WORLD number of this process

  int values[25] = { 829, 1, 24, 31, 43, 541, 6, 27, 8, 22, 434, 134, 753, 344, 224, 245, 754, 54, 35, 2, 32, 32, 42, 53, 21 };

  int res; // max will be here
  if (MPI_Allreduce_max(values[rank], &res, MPI_COMM_WORLD)) {
    printf("something bad has happend\n");
    MPI_Finalize();
    return -1;
  }
  printf("result from process %d is %d\n", rank, res);

  MPI_Finalize(); // finish this process
  return 0;
}
