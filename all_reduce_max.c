#include "all_reduce_max.h"
#include <math.h>
#include <stdio.h> 
#include <malloc.h>

typedef int bool;
#define true 1
#define false 0

static int handle_corner(int val_to_send, int rank, int first_neighbor, int second_neighbor, int n);
static int handle_horizontal_border(int val_to_send, int rank, int vertical_neighbor, int n);
static int handle_vertical_border(int val_to_send, int rank, int horizontal_neighbor, int n);
static int handle_middle(int val_to_send, int rank, int n);

static int handle_node(int val_to_send, bool do_first_send, int* neighbors, int neighbors_size);
static void send(int* pval_to_send, int* neighbors, int neighbors_size);
static void receive(int* results, int* neighbors, MPI_Request* requests, int neighbors_size);

static bool send_first(int rank, int n);

static int find_max(int* arr, int n, int initual);
static int max(int a, int b);

int MPI_Allreduce_max(const int send, int* res, MPI_Comm communicator) {
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank); // current process number
    MPI_Comm_size(MPI_COMM_WORLD, &size); // number of processes

    int n = round(sqrt(size));
    if (n != sqrt(size) || n < 2) {
        printf("not square matrix\n");
        return MPI_ERR_DIMS;
    }

    int longest_path = 2*n - 3; // number of the iterations we need minus 1
    *res = 0;


    int upper_left_corner = 0;
    int upper_right_corner = n - 1;
    int bottom_left_corner = n * (n - 1);
    int bottom_right_corner = n*n - 1;

    int corners[4] = {upper_left_corner, upper_right_corner, bottom_left_corner, bottom_right_corner};
    int corner_first_neighbors[4] = {rank + 1, rank - 1, rank + 1, rank - 1};
    int corner_second_neighbors[4] = {rank + n, rank + n, rank - n, rank - n};

    int i, j;
    // if we are one of the corners
    for (i = 0; i < 4; i++)
        if (corners[i] == rank) {
            *res = handle_corner(send, rank, corner_first_neighbors[i], corner_second_neighbors[i], n);
            for (j = 0; j < longest_path; j++)
                *res = handle_corner(*res, rank, corner_first_neighbors[i], corner_second_neighbors[i], n);
            return 0;
        }
    // if we are not a corner
    if ((rank > upper_left_corner && rank < upper_right_corner)
                || (rank > bottom_left_corner && rank < bottom_right_corner)) { // horizontal border
        int vertical_neighbor = (rank < upper_right_corner) ? (rank + n) : (rank - n);
        *res = handle_horizontal_border(send, rank, vertical_neighbor, n);      
        for (i = 0; i < longest_path; i++)
            *res = handle_horizontal_border(*res, rank, vertical_neighbor, n);

    } else if ((rank % n == 0) || ((rank - (n - 1)) % n == 0)) { // vertical border
        int horizontal_neighbor = (rank % n == 0) ? (rank + 1) : (rank - 1);
        *res = handle_vertical_border(send, rank, horizontal_neighbor, n);
        for (i = 0; i < longest_path; i++)
            *res = handle_vertical_border(*res, rank, horizontal_neighbor, n);

    } else { // middle guys
        *res = handle_middle(send, rank, n);
        for (i = 0; i < longest_path; i++)
            *res = handle_middle(*res, rank, n);
    }

    return 0;
}

static int handle_corner(int val_to_send, int rank, int first_neighbor, int second_neighbor, int n) {
    int neighbors[2] = {first_neighbor, second_neighbor};
    return handle_node(val_to_send, send_first(rank, n), neighbors, 2);
}

static int handle_horizontal_border(int val_to_send, int rank, int vertical_neighbor, int n) {
    int neighbors[3] = {rank - 1, rank + 1, vertical_neighbor};
    return handle_node(val_to_send, send_first(rank, n), neighbors, 3);
}

static int handle_vertical_border(int val_to_send, int rank, int horizontal_neighbor, int n) {
    int neighbors[3] = {rank - n, rank + n, horizontal_neighbor};
    return handle_node(val_to_send, send_first(rank, n), neighbors, 3);
}

static int handle_middle(int val_to_send, int rank, int n) {
    int neighbors[4] = {rank - n, rank + n, rank - 1, rank + 1};
    return handle_node(val_to_send, send_first(rank, n), neighbors, 4);
}

static int handle_node(int val_to_send, bool do_first_send, int* neighbors, int neighbors_size) {
    MPI_Request* requests = (MPI_Request*) malloc(sizeof(MPI_Request) * neighbors_size);
    int* results = (int*) malloc(sizeof(int) * neighbors_size);
    int new_max;

    if (do_first_send)
        send(&val_to_send, neighbors, neighbors_size);

    receive(results, neighbors, requests, neighbors_size);

    if (!do_first_send)
        send(&val_to_send, neighbors, neighbors_size);

    new_max = find_max(results, neighbors_size, val_to_send);
    free(results);
    free(requests);

    return new_max;
}

static void send(int* pval_to_send, int* neighbors, int neighbors_size) {
    int i;
    for (i = 0; i < neighbors_size; i++)
        MPI_Send(pval_to_send, 1, MPI_INT, neighbors[i], 0, MPI_COMM_WORLD);
}

static void receive(int* results, int* neighbors, MPI_Request* requests, int neighbors_size) {
    int i;
    MPI_Status status;

    for (i = 0; i < neighbors_size; i++)
        MPI_Irecv(results + i, 1, MPI_INT, neighbors[i], 0, MPI_COMM_WORLD, requests + i);
    for (i = 0; i < neighbors_size; i++)
        MPI_Wait(requests + i, &status);    
}

// we need to mark matrix in checkerboard order to avoid deadlocks
static bool send_first(int rank, int n) {
    bool matrix_is_even = n % 2 == 0;
    bool row_number_is_odd = (rank / n) % 2 == 1;
    return (rank + matrix_is_even*row_number_is_odd) % 2 == 0;
}

static int find_max(int* arr, int n, int initual) {
    int i;
    int res = initual;

    for (i = 0; i < n; i++)
        res = max(arr[i], res);
    return res;
}

static int max(int a, int b) {
    return (a) > (b) ? a : b;
}

