default: build

build:
	mpicc -g -lm main.c all_reduce_max.h all_reduce_max.c

run:
	mpirun -np 25 ./a.out

all: build run
